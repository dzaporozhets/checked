(ns checked.views.layout
  (:require [hiccup.page :refer [html5 include-css include-js]]
            [hiccup.element :refer [link-to]]
            [clavatar.core :refer [gravatar]]
            [noir.session :as session]))

(defn guest-menu []
  (list
    [:ul {:class "nav navbar-nav navbar-right"}
     [:li
      [:a {:class "login" :href "/login"} "Login"]]
     [:li
      [:a {:class "Register" :href "/register"} "Register"]]]))

(defn user-menu [user]
  (list
    [:ul {:class "nav navbar-nav navbar-left"}
     [:li
      [:a {:href "/issues"} "Issues"]]
     [:li
      [:a {:href "/projects"} "Projects"]]
     [:li
      [:a {:class "invites" :href "/invites"} "Invites"]]]
    [:ul {:class "nav navbar-nav navbar-right"}
     [:li
      [:a {:class "avatar user-link" :href "/profile"}
       [:img {:alt "avatar" :src (gravatar (session/get :user-email) :size 40)}]]]
     [:li
      [:a {:class "profile" :href "/profile"} "Profile"]]
     [:li
      [:a {:class "logout" :href "/logout"} "Logout"]]]))

(defn base [& content]
  (html5
    [:head
     [:meta {:http-equiv "content-type" :content "text/html; charset=UTF-8"}]
     [:meta {:name "description" :content "Absolutely simple project management tool"}]
     [:meta {:name "keywords" :content "images pictures"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
     [:title "Checked - Absolutely simple project management tool"]
     [:link {:rel "icon" :type "image/png" :sizes "96x96" :href "/img/checked-logo.png"}]
     [:link {:rel "apple-touch-icon" :type "image/png" :sizes "57x57" :href "/img/checked-logo.png"}]
     [:link {:rel "apple-touch-icon" :type "image/png" :sizes "72x72" :href "/img/checked-logo.png"}]
     [:link {:rel "apple-touch-icon" :type "image/png" :sizes "114x114" :href "/img/checked-logo.png"}]
     (include-css "/css/application.min.css")]
    [:body
     content
     [:script {:async "async" :type "text/javascript" :src "/js/application.min.js"}]]))

(defn common [& content]
  (base
    [:header.navbar.navbar-static-top.navbar-app.navbar-default
     [:div.container
      [:div.navbar-header
       [:a.navbar-brand {:href "/"}
        [:strong "checked"]]
       [:button.navbar-toggle.collapsed {:data-toggle "collapse" :data-target "#main-navbar"}
        [:span.sr-only "Toggle navigation"]
        [:span.icon-bar]
        [:span.icon-bar]
        [:span.icon-bar]]]
      [:nav.collapse.navbar-collapse {:id "main-navbar"}
       (if-let [user (session/get :user-id)]
         (user-menu user)
         (guest-menu))]]]
    [:div.container content]))
