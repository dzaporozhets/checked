(ns checked.models.comment
  (:require [clojure.java.jdbc :as sql]
            [environ.core :refer [env]]))

(def db
  (env :database-url))

(defn count-issue-comments [issue-id]
  (sql/query db
             ["SELECT COUNT(*) FROM comments WHERE issue_id = ?", issue-id]
             :result-set-fn first))

(defn get-issue-comments [issue-id]
  (sql/query db
             ["SELECT * FROM comments WHERE issue_id = ?", issue-id]))

(defn create-comment [comment]
  (first (sql/insert! db :comments comment)))
