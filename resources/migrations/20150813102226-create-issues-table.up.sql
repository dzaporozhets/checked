CREATE TABLE IF NOT EXISTS issues (
  id    SERIAL PRIMARY KEY,
  description text NOT NULL,
  user_id integer NOT NULL,
  project_id integer NOT NULL,
  open boolean NOT NULL DEFAULT true,
  timestamp timestamp DEFAULT current_timestamp
);
