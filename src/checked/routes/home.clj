(ns checked.routes.home
  (:require [compojure.core :refer :all]
            [checked.views.home :as view]
            [noir.util.route :refer [def-restricted-routes]]
            [checked.views.layout :as layout]))

(defn home []
  (layout/common (view/home)))

(def-restricted-routes home-routes
  (GET "/" [] (home)))
