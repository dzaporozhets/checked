(ns checked.views.home
  (:require [hiccup.element :refer :all]
            [checked.helpers :refer :all]
            [checked.models.invite :as invite-db]
            [checked.models.project :as project-db]
            [checked.models.issue :as issue-db]
            [noir.session :as session]))

(defn home []
  [:div
   [:h3.page-h (str "Hi, " (session/get :user-name))]
   [:p "This is your dashboard"]
   [:div.list-group
    (if-let [invites-count (:count (invite-db/count-invites (session/get :user-email)))]
      (if (pos? invites-count)
        [:a.list-group-item {:href "/invites" }
         [:h4.list-group-item-heading invites-count]
         " invite(s) to new project"]))
    (if-let [projects-count (:count (project-db/count-user-projects (current-user-id)))]
      (if (pos? projects-count)
        [:a.list-group-item {:href "/projects" }
         [:h4.list-group-item-heading projects-count]
         " active project(s)"]))
    (if-let [issues-count (:count (issue-db/count-authored-issues (current-user-id)))]
      (if (pos? issues-count)
        [:a.list-group-item {:href "/issues" }
         [:h4.list-group-item-heading issues-count]
         " issue(s) created by you"]))
    (if-let [issues-count (:count (issue-db/count-assignee-issues (current-user-id)))]
      (if (pos? issues-count)
        [:a.list-group-item {:href "/issues/assigned" }
         [:h4.list-group-item-heading issues-count]
         " issue(s) assigned to you"]))]
   [:div
    [:a {:class "btn btn-info" :href "/issues/new"}
     [:i.fa.fa-plus]
     [:span" New issue"]]]])
