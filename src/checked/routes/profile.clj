(ns checked.routes.profile
  (:require [compojure.core :refer :all]
            [noir.response :as resp]
            [noir.session :as session]
            [noir.util.route :refer [def-restricted-routes]]
            [noir.util.crypt :as crypt]
            [noir.session :as session]
            [checked.models.user :as db]
            [checked.views.layout :as layout]
            [checked.views.profile :as view]))

(defn update-password [current-password new-password confirm-password]
  (let [user (db/get-user-by-id (session/get :user-id))]
    (if (crypt/compare current-password (:encrypted_password user))
      (if (= new-password confirm-password)
        (do
          (db/update-user (:id user) {:encrypted_password (crypt/encrypt new-password)})
          (session/clear!)
          (resp/redirect "/login"))
        (str "Confirmation password does not match"))
      (str "Incorrect current password"))))


(defn remote-user [id]
  (db/delete-user id)
  (session/clear!)
  (resp/redirect "/"))

(defn profile-page []
  (layout/common
    (view/profile-page)))

(defn delete-profile []
  (remote-user (session/get :user-id)))

(def-restricted-routes profile-routes
  (GET "/profile" [] (profile-page))
  (POST "/profile/password/update" [current-password new-password confirm-password]
        (update-password current-password new-password confirm-password))
  (POST "/profile/delete" [] (delete-profile)))
