(ns checked.features.auth
  (:require [checked.handler :refer [app]]
            [checked.features.helper :refer :all]
            [kerodon.core :refer :all]
            [kerodon.test :refer :all]
            [clojure.test :refer :all]))

(use-fixtures :each
              (fn [f]
                (cleanup-db)
                (create-user)
                (f)))

(deftest user-login
  (-> (session app)
      (login)
      (within [:body]
        (has (some-text? "Hi, Foo")))))

(deftest user-signup
  (-> (session app)
      (visit "/")
      (follow-redirect)
      (follow "Register")
      (fill-in "Name" "Bar")
      (fill-in "Email" "bar@example.com")
      (fill-in "Password" "123456")
      (fill-in "Repeat password" "123456")
      (press "Create account")
      (follow-redirect)
      (within [:body]
        (has (some-text? "Hi, Bar")))))
