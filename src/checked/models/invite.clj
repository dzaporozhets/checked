(ns checked.models.invite
  (:require [clojure.java.jdbc :as sql]
            [environ.core :refer [env]]))

(def db
  (env :database-url))

(defn get-invite [id]
  (sql/query db
             ["SELECT * FROM invites WHERE id = ?", (Integer. id)]
             :result-set-fn first))

(defn get-user-invite [email id]
  (sql/query db
             ["SELECT * FROM invites
              WHERE email = ?
              AND id = ?", email, (Integer. id)]
             :result-set-fn first))

(defn count-invites [user-email]
  (sql/query db
             ["SELECT COUNT(*) FROM invites
              WHERE email = ?", user-email]
             :result-set-fn first))

(defn get-invites [user-email]
  (sql/query db
             ["SELECT * FROM invites
              WHERE email = ?
              ORDER BY id DESC", user-email]))

(defn get-project-invites [project-id]
  (sql/query db
             ["SELECT * FROM invites
              WHERE project_id = ?", project-id]))

(defn create-invite [invite]
  (first (sql/insert! db :invites invite)))

(defn delete-invite [id]
  (sql/delete! db :invites ["id = ?", id]))
