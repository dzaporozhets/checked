(ns checked.views.admin
  (:require [hiccup.element :refer :all]
            [checked.helpers :refer :all]
            [checked.models.user :as user-db]
            [checked.models.project :as project-db]
            [noir.session :as session]))

(defn home []
  [:div
   [:h3.page-h "Admin dashboard"]
   [:div.list-group
    (let [projects-count (:count (project-db/count))]
      [:div.list-group-item
       [:h4.list-group-item-heading projects-count]
       " project(s)"])
    (let [users-count (:count (user-db/count))]
      [:div.list-group-item
       [:h4.list-group-item-heading users-count]
       " user(s)"])]])
