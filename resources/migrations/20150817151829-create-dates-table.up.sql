CREATE TABLE IF NOT EXISTS dates (
  id    SERIAL PRIMARY KEY,
  due_date date NOT NULL,
  description text NOT NULL,
  user_id integer NOT NULL,
  project_id integer NOT NULL,
  timestamp timestamp DEFAULT current_timestamp
);

