(ns checked.routes.issues
  (:require [compojure.core :refer :all]
            [checked.access :refer :all]
            [checked.helpers :refer :all]
            [checked.models.issue :as db]
            [checked.models.project :as project-db]
            [checked.views.issues :as view]
            [checked.views.layout :as layout]
            [noir.util.route :refer [def-restricted-routes]]
            [ring.util.response :refer [not-found]]
            [noir.session :as session]
            [noir.response :as resp]))

(defn update-issue [project-id id description date_id assignee_id]
  (let [issue (db/get-project-issue project-id id)]
    (do
      (db/update-issue (Integer. id) {:description description
                                      :assignee_id (if-not (empty? assignee_id) (Integer. assignee_id) nil)
                                      :date_id (if-not (empty? date_id) (Integer. date_id) nil)})
      (resp/redirect (issue-path issue)))))

(defn close-issue [project-id id]
  (let [issue (db/get-project-issue project-id id)]
    (do
      (db/close-issue (Integer. id))
      (resp/redirect (issue-path issue)))))

(defn reopen-issue [project-id id]
  (let [issue (db/get-project-issue project-id id)]
    (do
      (db/reopen-issue (Integer. id))
      (resp/redirect (issue-path issue)))))

(defn issue-page [project-id id]
  (let [issue (db/get-project-issue project-id id)]
    (let [project (project-db/get-project-by-id (:project_id issue))]
      (layout/common (view/show project issue)))))

(defn edit-issue-page [project-id id]
  (let [issue (db/get-project-issue project-id id)]
    (let [project (project-db/get-project-by-id (:project_id issue))]
      (layout/common (view/edit project issue)))))

(defn new-issue-page [project-id]
  (let [project (project-db/get-project project-id)]
    (layout/common (view/new project))))

(defn create-issue [project-id description date_id assignee_id]
  (let [project (project-db/get-project project-id)]
    (let [issue (db/create-issue {:user_id (current-user-id)
                                  :project_id (Integer. project-id)
                                  :assignee_id (if-not (empty? assignee_id) (Integer. assignee_id) nil)
                                  :date_id (if-not (empty? date_id) (Integer. date_id) nil)
                                  :description description})]
      (resp/redirect (str "/project/" (:id project) "/issue/" (:id issue))))))

(defn user-authored-issues []
  (let [issues (db/get-user-issues (current-user-id))]
    (layout/common (view/user-issues issues "authored"))))

(defn user-assigned-issues []
  (let [issues (db/get-assigned-user-issues (current-user-id))]
    (layout/common (view/user-issues issues "assigned"))))

(defn select-project-page []
  (layout/common (view/select-project)))

(def-restricted-routes issues-routes
  (GET "/issues" []
       (user-authored-issues))
  (GET "/issues/assigned" []
       (user-assigned-issues))
  (GET "/issues/new" []
       (select-project-page))
  (GET "/project/:project-id/issue/:id" [project-id id]
       (issue-page project-id id))
  (GET "/project/:project-id/issue/:id/edit" [project-id id]
       (edit-issue-page project-id id))
  (GET "/project/:project-id/issues/new" [project-id]
       (new-issue-page project-id))
  (POST "/project/:project-id/issue/:id/update" [project-id id description date_id assignee_id]
        (update-issue project-id id description date_id assignee_id))
  (POST "/project/:project-id/issue/:id/close" [project-id id]
        (close-issue project-id id))
  (POST "/project/:project-id/issue/:id/reopen" [project-id id]
        (reopen-issue project-id id))
  (POST "/project/:project-id/issues/create" [project-id description date_id assignee_id]
        (create-issue project-id description date_id assignee_id)))
