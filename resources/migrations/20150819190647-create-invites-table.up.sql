CREATE TABLE IF NOT EXISTS invites (
  id    SERIAL PRIMARY KEY,
  user_id integer NOT NULL,
  project_id integer NOT NULL,
  email varchar(255) NOT NULL,
  timestamp timestamp DEFAULT current_timestamp
);
