jQuery(document).ready(function() {
  jQuery("time.timeago").timeago();
  $("#due_date").datepicker({dateFormat: "yy-mm-dd"});
});

$(document).on('keydown', '.js-cmd-enter textarea', function(e) {
  if(e.keyCode == 13 && (e.metaKey || e.ctrlKey)) {
    $(this).parents('form').submit()
  }
})

$(document).on('click', '.js-confirm', function(e) {
  if (!confirm("Are you sure?") == true) {
    e.preventDefault();
    return false;
  }
});
