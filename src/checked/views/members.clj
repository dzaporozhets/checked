(ns checked.views.members
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.element :refer :all]
            [hiccup.form :refer :all]
            [clj-time.coerce :as c]
            [clj-time.format :as f]
            [clavatar.core :refer [gravatar]]
            [checked.helpers :refer :all]
            [checked.models.user :as user-db]
            [checked.models.invite :as invite-db]
            [checked.models.issue :as issue-db]
            [checked.views.issues :as issue-view]
            [checked.views.projects :refer [project-head]]
            [checked.models.member :as db]
            [noir.session :as session]))

(defn invite-row [invite]
  [:li
   [:div
    [:div.author
     [:img {:src (gravatar (:email invite))}]
     [:strong (:email invite)]]
    [:span.light
     [:span "invite sent"]]]])

(defn member-row [member]
  [:li
   (if-let [user (user-db/get-user-by-id (:user_id member))]
     [:div
      [:div.author
       [:img {:src (gravatar (:email user))}]
       [:strong (:name user)]]
      [:span.light
       [:span " joined "]
       [:time.timeago {:datetime (c/from-date (:timestamp member))} (:timestamp member)]]])])

(defn members [project]
  [:div
   (project-head project)
   (project-menu project "people")
   [:div.row
    [:div.col-md-6
     [:ul.list.members-list
      (let [members (db/get-members (:id project))]
        (map member-row members))]]
    [:div.col-md-6
     [:ul.list
      (let [invites (invite-db/get-project-invites (:id project))]
        (map invite-row invites))]
     [:br]
     [:form {:action (project-path project "/invite") :method "POST"}
      [:div.form-group
       (label "email" "Email")
       (email-field {:class "form-control" :required true} "email")]
      [:button {:class "btn btn-success"} "Invite user"]]]]])
