CREATE TABLE IF NOT EXISTS members (
  id    SERIAL PRIMARY KEY,
  user_id integer NOT NULL,
  project_id integer NOT NULL,
  timestamp timestamp DEFAULT current_timestamp
);

