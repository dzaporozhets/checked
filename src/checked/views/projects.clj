(ns checked.views.projects
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.element :refer :all]
            [hiccup.form :refer :all]
            [clj-time.coerce :as c]
            [clj-time.format :as f]
            [checked.helpers :refer :all]
            [checked.models.comment :as comment-db]
            [checked.models.invite :as invite-db]
            [checked.models.user :as user-db]
            [checked.models.issue :as issue-db]
            [checked.models.project :as db]
            [checked.views.issues :as issue-view]
            [noir.session :as session]))

(defn breadcrumbs [project html]
  [:ol.breadcrumb
   [:li
    [:a {:href "/"} "Home"]]
   [:li
    [:a {:href (project-path project)} (escape-html (:name project))]]
   html])

(defn new-issue-row [project]
  (list
    [:li.pull-right
     [:a {:class "btn btn-sm btn-info" :href (project-path project "/issues/new")}
      [:i.fa.fa-plus]
      [:span.hidden-xs " New issue"]]]))

(defn settings [project]
  [:div
   (breadcrumbs project
                [:li.active
                 [:span "Settings"]])
   [:div.row
    [:div.col-md-6
     (form-to [:post (project-path project "/update")]
              (input-control text-field "name" "Name" (:name project))
              (input-control text-field "description" "Description" (:description project))
              (submit-button {:class "btn btn-success"} "Update project"))
     [:br]]
    [:div.col-md-6
     [:div
      [:p "When you are done with project you can archive project"]
      (form-to [:post (project-path project "/archive")]
               (submit-button {:class "btn btn-sm btn-warning"} "Archive project"))]
     [:hr]
     [:div
      [:p "Deleted project can not be restored"]
      [:form {:action (project-path project "/delete") :method "POST"}
       [:button {:class "btn btn-danger btn-sm js-confirm"} "Delete project"]]]]]])

(defn project-head [project]
  [:div
   [:div.pull-right
    [:a {:class "btn btn-default btn-sm" :title "Project settings" :href (project-path project "/settings")}
     [:i.fa.fa-cog]
     [:span.hidden-xs " Settings"]]]
   [:h3.page-h (escape-html (:name project))]
   [:p
    (text-to-html (:description project))]])

(defn show [project filter]
  [:div
   (project-head project)
   (project-menu project "issues" (new-issue-row project))
   [:div
    [:ul.nav.nav-text-pills
     [:li {:class (if (:open filter) "active")}
      [:a {:href (project-path project)} "Open"]]
     [:li {:class (if-not (:open filter) "active")}
      [:a {:href (project-path project "/issues/closed")} "Closed"]]]
    [:ul.list.issues-list
     (let [issues (issue-db/get-project-issues (:id project) filter)]
       (map issue-view/issue-row issues))]]])

(defn new []
  [:div.center-form
   [:h3.page-h "New project"]
   (form-to [:post "/projects/create"]
            (input-control text-field "name" "Name")
            (input-control text-field "description" "Description")
            (submit-button {:class "btn btn-success"} "Create project"))])

(defn new-project-row []
  (list
    [:div.pull-right
     [:a.btn.btn-sm.btn-info {:href "/projects/new"}
      [:span "New project"]]]))

(defn project-row [project]
  (list
    [:li
     (if (:archived project)
       [:span.closed-label.text-warning
        [:i.fa.fa-archive]])
     [:a.name {:href (str "/project/" (:id project))}
      [:strong (escape-html (:name project))]]
     [:div.description (escape-html (:description project))]]))

(defn projects [filter]
  [:div
   (new-project-row)
   [:h3.page-h "Your Projects"]
   [:hr]
   [:div
    [:ul.nav.nav-text-pills
     [:li {:class (if-not (:archived filter) "active")}
      [:a {:href "/"} "Active"]]
     [:li {:class (if (:archived filter) "active")}
      [:a {:href "/projects/archived"} "Archived"]]]]
   (let [user (session/get :user-id)]
     (list
       [:ul.list
        (let [projects (db/get-projects-for-user (Integer. user) filter)]
          (map project-row projects))]))])
