(ns checked.views.files
  (:require [hiccup.element :refer :all]
            [checked.models.project :as db]
            [clavatar.core :refer [gravatar]]
            [clojure.contrib.humanize :refer [filesize]]
            [checked.models.user :as user-db]
            [checked.models.file :as file-db]
            [clj-time.coerce :as c]
            [clj-time.format :as f]
            [checked.helpers :refer :all]
            [checked.views.projects :refer [project-head]]
            [noir.session :as session]))

(defn file-row [file]
  [:li
   [:div.light
    (if-let [author (user-db/get-user-by-id (:user_id file))]
      [:span
       [:span.author
        [:img {:src (gravatar (:email author))}]
        [:span (:name author)]]
       " uploaded "])
    [:span.hidden-xs
     [:time.timeago {:datetime (c/from-date (:timestamp file))} (:timestamp file)]]]
   [:strong
    [:a {:href (file-uri (:project_id file) (:name file))}
     (:name file)]
    [:div.pull-right
     (filesize (:size file))]]])

(defn files [project]
  [:div
   (project-head project)
   (project-menu project "files")
   [:ul.list.files-list
    (let [files (file-db/get-project-files (:id project))]
      (map file-row files))]])
