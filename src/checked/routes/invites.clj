(ns checked.routes.invites
  (:require [compojure.core :refer :all]
            [checked.access :refer :all]
            [checked.helpers :refer :all]
            [checked.models.member :as member-db]
            [checked.models.user :as user-db]
            [checked.models.project :as project-db]
            [checked.models.invite :as invite-db]
            [checked.views.invites :as view]
            [checked.views.layout :as layout]
            [checked.mailer :refer [send-invite-email]]
            [postal.core :refer [send-message]]
            [noir.util.route :refer [def-restricted-routes]]
            [noir.response :as resp]
            [noir.session :as session]))

(defn invites-page []
  (layout/common (view/invites)))

(defn accept-invite [id]
  (if-let [invite (invite-db/get-invite id)]
    (if-let [member (member-db/create-member {:user_id (current-user-id)
                                              :project_id (:project_id invite)})]
      (do
        (invite-db/delete-invite (:id invite))
        (resp/redirect "/invites")))))

(defn decline-invite [id]
  (if-let [invite (invite-db/get-invite id)]
    (do
      (invite-db/delete-invite (:id invite))
      (resp/redirect "/invites"))))

(defn create-invite [project-id email]
  (let [project (project-db/get-project project-id)]
    (let [invite (invite-db/create-invite {:user_id (current-user-id)
                                           :project_id (Integer. project-id)
                                           :email email})]
      (send-invite-email email (current-user) project)
      (resp/redirect (project-path project "/people")))))

(def-restricted-routes invites-routes
  (GET "/invites" [] (invites-page))
  (POST "/invite/:id/accept" [id] (accept-invite id))
  (POST "/invite/:id/decline" [id] (decline-invite id))
  (POST "/project/:project-id/invite" [project-id email]
        (create-invite project-id email)))
