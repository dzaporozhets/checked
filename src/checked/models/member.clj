(ns checked.models.member
  (:require [clojure.java.jdbc :as sql]
            [environ.core :refer [env]]))

(def db
  (env :database-url))

(defn get-member [id]
  (sql/query db
             ["SELECT * FROM members 
              WHERE id = ?", id]))

(defn get-project-member [project-id user-id]
  (sql/query db
             ["SELECT * FROM members 
              WHERE project_id = ? 
              AND user_id = ?", project-id, user-id]
             :result-set-fn first))

(defn get-members [project-id]
  (sql/query db
             ["SELECT * FROM members 
              WHERE project_id = ?", project-id]))

(defn create-member [member]
  (first (sql/insert! db :members member)))
