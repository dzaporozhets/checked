(ns checked.routes.projects
  (:require [compojure.core :refer :all]
            [checked.access :refer :all]
            [checked.helpers :refer :all]
            [checked.models.project :as db]
            [checked.models.member :as member-db]
            [checked.views.layout :as layout]
            [checked.views.projects :as view]
            [noir.session :as session]
            [noir.util.route :refer [def-restricted-routes]]
            [noir.response :as resp]))


(defn settings-page [id]
  (let [project (db/get-project id)]
    (layout/common (view/settings project))))

(defn project-page [id filter]
  (let [project (db/get-project id)]
    (layout/common (view/show project filter))))

(defn new-project-page []
  (layout/common (view/new)))

(defn create-project [name description]
  (let [project (db/create-project {:name name :description description})]
    (do
      (member-db/create-member {:user_id (current-user-id)
                                :project_id (:id project)})
      (resp/redirect (project-path project)))))

(defn update-project [id name description]
  (let [project (db/get-project id)]
    (do
      (db/update-project (Integer. id) {:name name :description description})
      (resp/redirect (project-path project)))))

(defn archive-project [id]
  (let [project (db/get-project id)]
    (do
      (db/archive-project (Integer. id))
      (resp/redirect "/"))))

(defn delete-project [id]
  (let [project (db/get-project id)]
    (do
      (db/delete-project (Integer. id))
      (resp/redirect "/"))))

(defn projects-page [filter]
  (layout/common (view/projects filter)))

(def-restricted-routes projects-routes
  (GET "/projects" []
       (projects-page {:archived false}))
  (GET "/projects/archived" []
       (projects-page {:archived true}))
  (GET "/project/:project-id" [project-id]
       (project-page project-id {:open true}))
  (GET "/project/:project-id/settings" [project-id]
       (settings-page project-id))
  (GET "/project/:project-id/issues" [project-id]
       (project-page project-id {:open true}))
  (GET "/project/:project-id/issues/closed" [project-id]
       (project-page project-id {:open false}))
  (GET "/projects/new" []
       (new-project-page))
  (POST "/projects/create" [name description]
        (create-project name description))
  (POST "/project/:project-id/update" [project-id name description]
        (update-project project-id name description))
  (POST "/project/:project-id/archive" [project-id]
        (archive-project project-id))
  (POST "/project/:project-id/delete" [project-id]
        (delete-project project-id)))
