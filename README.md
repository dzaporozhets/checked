# Checked

[![build status](https://gitlab.com/dzaporozhets/checked/badges/master/build.svg)](https://gitlab.com/dzaporozhets/checked/commits/master)

Absolutely simple project management tool. 

![screenshot-phone](https://gitlab.com/dzaporozhets/checked/uploads/a1ad9e5d5ac69e42004cf51f3819fea5/screenshot-phone.png)

## About

I made this project for learning Clojure language. 
It is pretty basic web application with minimum amount of javascript and mobile friendly UI. 
It might not be ready for production use.

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

You will need PostgreSQL database.

## Development

Install PostreSQL and create database. Details of database connection now 
are in `project.clj`. You can override it by passing `DATABASE_URL` command

    createdb checked-dev


Run migrations

    lein migratus migrate


To start a web server for the application, run:

    lein ring server

To run tests you need to setup test database

    # create separate database for tests
    createdb checked-test

    # pass database url to migration
    DATABASE_URL=postgresql://localhost:5432/checked-test lein migratus migrate

    # run tests
    DATABASE_URL=postgresql://localhost:5432/checked-test lein test

Tu run code quality checks

    lein checkall


## Deploy

### Heroku

The easiest way to try this application is to deploy it to Heroku:

    git clone https://gitlab.com/dzaporozhets/checked.git
    cd checked
    heroku create
    heroku config:set EMAIL_FROM=noreply@example.com
    heroku addons:create heroku-postgresql:hobby-dev
    git push heroku master
    heroku run lein migratus migrate
    heroku open

### Own server

In order to make it work for production environment you need next:

* postgresql, postfix and jre
* pass `DATABASE_URL` with valid url to database
* pass `PORT` number
* pass `EMAIL_FROM` with valid email address of server
* build jar package: `lein ring uberjar`
* run package on JRE: `java -jar checked.jar`

The result command to run application can be next: 

    EMAIL_FROM=noreply@example.com PORT=8000 DATABASE_URL=postgres://LOGIN:PASSWORD@localhost:5432/checked-production java -jar checked.jar 


## License

The MIT License (MIT)

See [LICENSE](https://gitlab.com/dzaporozhets/checked/blob/master/LICENSE) file
