CREATE UNIQUE INDEX index_members_on_project_id_and_user_id ON members USING btree (project_id, user_id);
