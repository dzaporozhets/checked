(ns checked.models.project
  (:require [clojure.java.jdbc :as sql]
            [environ.core :refer [env]]))

(def db
  (env :database-url))

(defn get-projects []
  (sql/query db "SELECT * FROM projects"))

(defn get-projects-for-user [user-id filter]
  (sql/query db
             ["SELECT * FROM projects
              INNER JOIN members
              ON members.project_id = projects.id
              WHERE members.user_id = ?
              AND projects.archived = ? ", user-id, (:archived filter)]))

(defn count []
  (sql/query db
             ["SELECT COUNT(*) FROM projects"]
             :result-set-fn first))

(defn count-user-projects [user-id]
  (sql/query db
             ["SELECT COUNT(*) FROM projects
              INNER JOIN members
              ON members.project_id = projects.id
              WHERE members.user_id = ?
              AND projects.archived = false", user-id]
             :result-set-fn first))

(defn create-project [project]
  (first (sql/insert! db :projects project)))

(defn get-project-by-path [email]
  (sql/query db
             ["SELECT * FROM projects WHERE path = ?", email]
             :result-set-fn first))

(defn get-project-by-id [id]
  (sql/query db
             ["SELECT * FROM projects WHERE id = ?", (Integer. id)]
             :result-set-fn first))

(defn update-project [id params]
  (sql/update! db :projects params ["id = ?" id]))

(defn archive-project [id]
  (sql/update! db :projects {:archived true} ["id = ?" id]))

(defn delete-project [id]
  (do
    (sql/delete! db :projects ["id = ?", id])
    (sql/delete! db :issues ["project_id = ?", id])
    (sql/delete! db :comments ["project_id = ?", id])
    (sql/delete! db :files ["project_id = ?", id])
    (sql/delete! db :dates ["project_id = ?", id])
    (sql/delete! db :invites ["project_id = ?", id])
    (sql/delete! db :members ["id = ?", id])))

(defn get-project [id]
  (get-project-by-id id))

