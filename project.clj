(defproject checked "0.1.0-SNAPSHOT"
  :description "Absolutely simple project management tool"
  :url "https://gitlab.com/dzaporozhets/checked"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.3.1"]
                 [hiccup "1.0.5"]
                 [postgresql/postgresql "9.1-901.jdbc4"]
                 [org.clojure/java.jdbc "0.3.7"]
                 [lib-noir "0.7.6"]
                 [clj-time "0.10.0"]
                 [migratus "0.8.1"]
                 [prone "0.8.2"]
                 [bilus/clojure-humanize "0.1.0-SNAPSHOT" :exclusions [clj-time]]
                 [environ "1.0.0"]
                 [com.draines/postal "1.11.3"]
                 [clavatar "0.2.1"]
                 [ring-logger "0.7.4"]
                 [ring/ring-defaults "0.1.2"]]
  :plugins [[lein-ring "0.9.2"]
            [lein-environ "1.0.0"]
            [lein-asset-minifier "0.2.3"]
            [lein-checkall "0.1.1"]
            [migratus-lein "0.1.5"]]
  :migratus {:store :database
             :migration-dir "migrations"
             :db (or (System/getenv "DATABASE_URL") "postgresql://localhost:5432/checked-dev")}
  :ring {:handler checked.handler/app
         :init checked.handler/init}
  :minify-assets
  {:assets
   {"resources/public/css/application.min.css" ["resources/dev/css/bootstrap.min.css"
                                                "resources/dev/css/jquery-ui.min.css"
                                                "resources/dev/css/application.css"
                                                "resources/dev/css/font-awesome.min.css"]
    "resources/public/js/application.min.js" ["resources/dev/js/jquery-1.11.3.min.js"
                                              "resources/dev/js/jquery-ui.min.js"
                                              "resources/dev/js/bootstrap.min.js"
                                              "resources/dev/js/jquery.timeago.js"
                                              "resources/dev/js/application.js"]}}
  :profiles
  {:test {:env {:database-url "postgresql://localhost:5432/checked-test" :email-from "noreply@example.com"}}
   :dev {
         :env {:database-url "postgresql://localhost:5432/checked-dev" :email-from "noreply@example.com"}
         :dependencies [[javax.servlet/servlet-api "2.5"]
                        [kerodon "0.6.0"]
                        [ring-mock "0.1.5"]]
         :ring {:stacktrace-middleware prone.middleware/wrap-exceptions}}})
