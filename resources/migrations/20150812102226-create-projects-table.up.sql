CREATE TABLE IF NOT EXISTS projects (
  id    SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL UNIQUE,
  description varchar(255) NOT NULL,
  timestamp timestamp DEFAULT current_timestamp
);
