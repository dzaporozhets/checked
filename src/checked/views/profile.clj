(ns checked.views.profile
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.form :refer :all]
            [hiccup.element :refer :all]
            [checked.helpers :refer :all]
            [clj-time.coerce :as c]
            [clj-time.format :as f]
            [clavatar.core :refer [gravatar]]
            [checked.models.user :as db]
            [noir.session :as session]))

(defn profile-page []
  (let [user (db/get-user-by-id (session/get :user-id))]
    [:div
     [:ol.breadcrumb
      [:li
       [:a {:href "/"} "Home"]]
      [:li.active
       [:span "Your profile"]]]
     [:div.row
      [:div.col-md-6
       [:div {:class "user-info"}
        [:p
         [:img.thumbnail {:src (gravatar (:email user) :size 200)}]
         [:span.light "You can change your avatar at gravatar.com"]]
        [:p
         [:span "Name: "]
         [:strong (:name user)]]
        [:p
         [:span "Email: "]
         [:strong (:email user)]]
        [:p
         [:span "Member since: "]
         [:strong (f/unparse (f/formatters :date) (c/from-date (:timestamp user)))]]]]
      [:div.col-md-6
       (form-to [:post "/profile/password/update"]
                (input-control password-field "current-password" "Current password")
                (input-control password-field "new-password" "New password")
                (input-control password-field "confirm-password" "Confirm new password")
                (submit-button {:class "btn btn-primary"} "Change password"))
       [:hr]
       [:form {:action "/profile/delete" :method "POST"}
        [:p "Deleted account can not be restored"]
        [:button {:class "js-confirm btn btn-danger btn-sm"} "Delete account"]]]]]))
