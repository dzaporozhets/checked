CREATE TABLE IF NOT EXISTS files (
  id    SERIAL PRIMARY KEY,
  name  varchar(255) NOT NULL,
  size integer NOT NULL,
  user_id integer NOT NULL,
  comment_id integer NOT NULL,
  project_id integer NOT NULL,
  timestamp timestamp DEFAULT current_timestamp
);

