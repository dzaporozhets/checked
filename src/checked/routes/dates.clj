(ns checked.routes.dates
  (:require [compojure.core :refer :all]
            [checked.access :refer :all]
            [checked.helpers :refer :all]
            [checked.models.date :as db]
            [checked.models.project :as project-db]
            [checked.views.dates :as view]
            [checked.views.layout :as layout]
            [noir.util.route :refer [def-restricted-routes]]
            [ring.util.response :refer [not-found]]
            [clj-time [format :as timef] [coerce :as timec]]
            [noir.session :as session]
            [noir.response :as resp]))

(defn parse-date [date]
  (->> date
      (str)
      (timef/parse (timef/formatter "yyyy-MM-dd"))
      timec/to-timestamp))

(defn dates-page [project-id filter]
  (let [project (project-db/get-project project-id)]
    (layout/common (view/dates project filter))))


(defn date-page [project-id id]
  (let [project (project-db/get-project project-id)]
    (let [date (db/get-date-by-id (Integer. id))]
      (layout/common (view/show project date)))))

(defn new-date-page [project-id]
  (let [project (project-db/get-project project-id)]
    (layout/common (view/new project))))

(defn create-date [project-id due-date description]
  (let [project (project-db/get-project project-id)]
    (let [date (db/create-date {:due_date (parse-date due-date)
                                :user_id (current-user-id)
                                :project_id (Integer. project-id)
                                :description description})]
      (resp/redirect (str "/project/" (:id project) "/date/" (:id date))))))

(def-restricted-routes dates-routes
  (GET "/project/:project-id/dates" [project-id] (dates-page project-id {:future true}))
  (GET "/project/:project-id/dates/past" [project-id] (dates-page project-id {:future false}))
  (GET "/project/:project-id/date/:id" [project-id id] (date-page project-id id))
  (GET "/project/:project-id/dates/new" [project-id] (new-date-page project-id))
  (POST "/project/:project-id/dates/create" [project-id due_date description] (create-date project-id due_date description)))
