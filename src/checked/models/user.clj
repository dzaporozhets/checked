(ns checked.models.user
  (:require [clojure.java.jdbc :as sql]
            [environ.core :refer [env]]))

(def db
  (env :database-url))

(defn create-user [user]
  (first (sql/insert! db :users user)))

(defn get-project-users [project-id]
  (sql/query db ["SELECT * FROM users
                 INNER JOIN members
                 ON members.user_id = users.id
                 WHERE members.project_id = ?", project-id]))

(defn get-user-by-email [email]
  (sql/query db
             ["SELECT * FROM users WHERE email = ?", email]
             :result-set-fn first))

(defn get-user-by-id [id]
  (sql/query db
             ["SELECT * FROM users WHERE id = ?", id]
             :result-set-fn first))

(defn count []
  (sql/query db
             ["SELECT COUNT(*) FROM users"]
             :result-set-fn first))

(defn delete-user [id]
  (sql/delete! db :users ["id = ?", id]))

(defn delete-user-by-email [email]
  (sql/delete! db :users ["email = ?", email]))

(defn update-user [id params]
  (sql/update! db :users params ["id = ?" id]))
