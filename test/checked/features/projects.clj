(ns checked.features.projects
  (:require [checked.handler :refer [app]]
            [checked.features.helper :refer :all]
            [kerodon.core :refer :all]
            [kerodon.test :refer :all]
            [clojure.test :refer :all]))

(use-fixtures :each
              (fn [f]
                (cleanup-db)
                (create-user)
                (f)))

(deftest create-project
  (-> (session app)
      (login)
      (visit "/projects/new")
      (fill-in "Name" "test-123")
      (fill-in "Description" "awesome project")
      (press "Create project")
      (follow-redirect)
      (within [:h3.page-h]
        (has (some-text? "test-123")))))
