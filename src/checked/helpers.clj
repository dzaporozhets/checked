(ns checked.helpers
  (:require [compojure.core :refer :all]
            [ring.util.codec :refer [url-encode]]
            [checked.models.user :as user-db]
            [hiccup.form :refer :all]
            [hiccup.element :refer :all]
            [noir.session :as session]
            [environ.core :refer [env]]))

(def uploads-path "resources/uploads/")

(defn dates-path [project & [suffix]]
  (str "/project/" (:id project) "/dates" suffix))

(defn project-path [project & [suffix]]
  (str "/project/" (:id project) suffix))

(defn date-path [date & [suffix]]
  (str "/project/" (:project_id date) "/date/" (:id date) suffix))

(defn issue-path [issue & [suffix]]
  (str "/project/" (:project_id issue) "/issue/" (:id issue) suffix))

(defn file-uri [project-id file-name]
  (str "/files/" project-id "/" (url-encode file-name)))

(defn current-user-id []
  (Integer. (session/get :user-id)))

(defn current-user []
  (user-db/get-user-by-id (current-user-id)))

(defn project-menu [project active & html]
  [:ul.nav.nav-pills.project-menu
   [:li {:class (if (= "issues" active) "active")}
    [:a {:href (project-path project)} "Issues"]]
   [:li {:class (if (= "files" active) "active")}
    [:a {:href (project-path project "/files")} "Files"]]
   [:li {:class (if (= "dates" active) "active")}
    [:a {:href (project-path project "/dates")} "Dates"]]
   [:li {:class (if (= "people" active) "active")}
    [:a {:href (project-path project "/people")} "People"]]
   html])

(defn newline-to-br [s]
  (clojure.string/replace s #"\r\n|\n|\r" "<br />\n"))

(defn input-control [type id name & [value]]
  [:div.form-group
   (list
     (label id name)
     (type {:class "form-control"} id value))])

(defn escape-html [text]
  (clojure.string/escape text
                         {\& "&amp;"
                          \< "&lt;"
                          \> "&gt;"
                          \" "&quot;"
                          \' "&#39;"}))

(defn auto-link [text]
  (clojure.string/replace
    text
    #"https?://[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]"
    #(str "<a rel=\"nofollow\" href=\"" % "\">" % "</a>")))

(defn text-to-html [text]
  (newline-to-br (auto-link (escape-html text))))
