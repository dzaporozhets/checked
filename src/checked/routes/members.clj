(ns checked.routes.members
  (:require [compojure.core :refer :all]
            [checked.helpers :refer :all]
            [checked.access :refer :all]
            [checked.models.project :as project-db]
            [checked.models.member :as member-db]
            [checked.views.layout :as layout]
            [noir.util.route :refer [def-restricted-routes]]
            [checked.views.members :as view]
            [noir.session :as session]
            [noir.response :as resp]))


(defn members-page [project-id]
  (let [project (project-db/get-project project-id)]
    (layout/common (view/members project))))

(def-restricted-routes members-routes
  (GET "/project/:project-id/people" [project-id] (members-page project-id)))
