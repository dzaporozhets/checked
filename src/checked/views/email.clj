(ns checked.views.email
  (:require [hiccup.core :refer [html]]
            [hiccup.element :refer :all]))

(defn invite [inviter project]
  (html
    [:body
     [:p
      (:name inviter)
      " invited to join the project: "
      (:name project)]
     [:hr]
     [:p "This is automatic message. No need to respond"]]))


(defn new-comment [author comment project comment-url]
  (html
    [:body
     [:p
      [:strong (:name author)]]
     [:p
      (:comment comment)]
     [:p
      [:a {:class "comment-link" :href comment-url} "View it on web"]]
     [:hr]
     [:p "This is automatic message. No need to respond"]]))
