(ns checked.views.dates
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.element :refer :all]
            [hiccup.form :refer :all]
            [clj-time.coerce :as c]
            [clj-time.format :as f]
            [clavatar.core :refer [gravatar]]
            [checked.helpers :refer :all]
            [checked.models.user :as user-db]
            [checked.models.issue :as issue-db]
            [checked.views.issues :as issue-view]
            [checked.views.projects :refer [project-head]]
            [checked.models.date :as db]
            [noir.session :as session]))

(defn new-date-row [project]
  (list
    [:li.pull-right
     [:a.btn.btn-info.btn-sm {:href (project-path project "/dates/new")}
      [:i.fa.fa-plus]
      [:span.hidden-xs " New date"]]]))

(defn breadcrumbs [project html]
  [:ol.breadcrumb
   [:li
    [:a {:href "/"} "Home"]]
   [:li
    [:a {:href (project-path project)} (:name project)]]
   html])

(defn show [project date]
  [:div
   (breadcrumbs project
                [:li.active
                 [:span (str "Date #" (:id date))]])
   [:div {:class "date-info"}
    [:h4.date-description
     (:due_date date)
     " - "
     (:description date)]
    [:p.light
     (if-let [author (user-db/get-user-by-id (:user_id date))]
       [:span
        [:span.author
         [:span (str (:name author) " ")]]])
     [:span " created this date "]
     [:time.timeago {:datetime (c/from-date (:timestamp date))} (:timestamp date)]]]
   [:hr]
   [:ul.list.issues-list
    (let [issues (issue-db/get-date-issues (:id date) {:open true})]
      (map issue-view/issue-row issues))]])

(defn new [project]
  [:div.center-form
   (breadcrumbs project
                [:li.active
                 [:span "New date"]])

   (form-to {:class "js-cmd-enter"} [:post (project-path project "/dates/create")]
            [:div.form-group
             (label "due_date" "Date (yyyy-mm-dd)")
             (text-field {:class "form-control"} "due_date")]
            [:div.form-group
             (label "description" "Description")
             (text-area {:class "form-control" :rows 6} "description")]
            (submit-button {:class "btn btn-success"} "Create date"))])

(defn date-row [date]
  [:li
   [:a.name.title {:href (date-path date)}
    [:strong
     (:due_date date)]]
   [:div.light
     (first (.split (:description date) "\n"))]])

(defn dates [project filter]
  [:div
   (project-head project)
   (project-menu project "dates" (new-date-row project))
   [:ul.nav.nav-text-pills
    [:li {:class (if (:future filter) "active")}
     [:a {:href (dates-path project)} "Future"]]
    [:li {:class (if-not (:future filter) "active")}
     [:a {:href (dates-path project "/past")} "Past"]]]
   [:ul.list.dates-list
    (let [dates (db/get-project-dates (:id project) filter)]
      (map date-row dates))]])
