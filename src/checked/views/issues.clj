(ns checked.views.issues
  (:require [hiccup.element :refer :all]
            [hiccup.form :refer :all]
            [clj-time.coerce :as c]
            [clj-time.format :as f]
            [clavatar.core :refer [gravatar]]
            [checked.helpers :refer :all]
            [checked.models.comment :as comment-db]
            [checked.models.user :as user-db]
            [checked.models.file :as file-db]
            [checked.models.date :as date-db]
            [checked.models.issue :as db]
            [checked.models.project :as project-db]
            [noir.session :as session]))

(defn issue-row [issue]
  [:li
   [:a.name.title {:href (issue-path issue)}
    (if-not (:open issue)
      [:span.closed-label.text-danger
       [:i.fa.fa-check]])
    [:strong
     (escape-html (first (.split (:description issue) "\n")))]]
   [:div.light
    (if-let [comments (:count (comment-db/count-issue-comments (:id issue)))]
      (if (pos? comments)
        [:span.pull-right
         [:i.fa.fa-comment]
         " "
         [:span comments]]))
    (list
      [:span.hidden-xs "created "]
      [:time.timeago.hidden-xs {:datetime (c/from-date (:timestamp issue))} (:timestamp issue)])
    (if-let [author (user-db/get-user-by-id (:user_id issue))]
      (list
        " by "
        [:span (escape-html (:name author))]))]])

(defn comment-row [comment]
  (list
    [:li {:id (str "comment-" (:id comment)) :class "comment"}
     [:p.comment-head
      (if-let [author (user-db/get-user-by-id (:user_id comment))]
        [:span.author
         [:img {:src (gravatar (:email author))}]
         [:span (escape-html (:name author))]])
      [:span.light
       [:span.hidden-xs " commented"]
       [:span " "]
       [:a.light {:href (str "/project/" (:project_id comment) "/issue/" (:issue_id comment) "#comment-" (:id comment))}
        [:time.timeago {:datetime (c/from-date (:timestamp comment))} (:timestamp comment)]]]]
     [:div.comment-text
      (text-to-html (:comment comment))]
     (if-let [file (file-db/get-file-by-comment (:id comment))]
       [:div.attachment
        [:a {:href (file-uri (:project_id file) (:name file))}
         (if (re-find #"\.(jpg|png|jpeg)$" (:name file))
           [:img {:class "thumbnail" :src (file-uri (:project_id file) (:name file)) :alt (:name file)}]
           (:name file))]])]))

(defn close-issue-form [issue]
  [:div.close-form
   (form-to [:post (issue-path issue "/close")]
            (submit-button {:class "btn btn-danger"} "Close"))])

(defn reopen-issue-form [issue]
  [:div.close-form
   (form-to [:post (issue-path issue "/reopen")]
            (submit-button {:class "btn btn-default"} "Reopen"))])

(defn comment-form [issue]
  [:div#comment-form.comment-form-holder
   (form-to {:enctype "multipart/form-data" :class "js-cmd-enter"} [:post (issue-path issue "/comment")]
            [:div.form-group
             (label "comment" "Comment")
             (text-area {:class "form-control" :rows 3 :required true} "comment")]
            [:div.form-group
             (list
               (file-upload :file))]
            (submit-button {:class "btn btn-success"} "Comment"))])

(defn breadcrumbs [project html]
  [:ol.breadcrumb
   [:li
    [:a {:href "/"} "Home"]]
   [:li
    [:a {:href (project-path project)} (:name project)]]
   html])

(defn date-to-option [date]
  [(str (:due_date date) " - " (:description date)), (:id date)])

(defn user-to-option [user]
  [(:name user), (:id user)])

(defn edit [project issue]
  [:div
   (breadcrumbs project
                [:li.active
                 [:span (str "Issue #" (:id issue))]])
   [:div.center-form
    (form-to {:class "js-cmd-enter"} [:post (issue-path issue "/update")]
             [:div.form-group
              (label "description" "Description")
              (text-area {:class "form-control" :rows 6} "description" (:description issue))]
             (let [users (user-db/get-project-users (:project_id issue))]
               (if (seq users)
                 [:div.form-group
                  (label "assignee_id" "Assignee")
                  [:select {:class "form-control" :name "assignee_id"}
                   [:option {:value ""} "None"]
                   (select-options (map user-to-option users) (:assignee_id issue))]]))
             (let [dates (date-db/get-project-dates (:project_id issue) {:future true})]
               (if (seq dates)
                 [:div.form-group
                  (label "date_id" "Due date")
                  [:select {:class "form-control" :name "date_id"}
                   [:option {:value ""} "None"]
                   (select-options (map date-to-option dates) (:date_id issue))]]))
             (submit-button {:class "btn btn-success"} "Update issue"))]])

(defn show [project issue]
  [:div
   (breadcrumbs project
                [:li.active
                 [:span (str "Issue #" (:id issue))]])
   [:div.list-group.issue-info
    [:div.list-group-item {:class (if (:open issue) "list-group-item-success" "list-group-item-danger")}
     [:a {:href (issue-path issue "/edit") :class "edit-issue-link"}
      [:i.fa.fa-pencil]
      " Edit"]
     (if (:open issue)
       [:div
        [:i.fa.fa-exclamation-circle]
        " Open"]
       [:div
        [:i.fa.fa-check]
        " Closed"])]
    [:div.list-group-item
     [:div.issue-description
      (text-to-html (:description issue))]]
    (if-let [author (user-db/get-user-by-id (:user_id issue))]
      [:div.list-group-item
       [:span "Created by "]
       [:span.author
        [:span (escape-html (:name author))]]
       [:span " "]
       [:time.timeago {:datetime (c/from-date (:timestamp issue))} (:timestamp issue)]])
    (if-let [assignee (user-db/get-user-by-id (:assignee_id issue))]
      [:div.list-group-item
       [:span "Assigned to "]
       [:span.author
        [:span (escape-html (:name assignee))]]])
     (if-let [date (date-db/get-date-by-id (:date_id issue))]
       [:div.list-group-item
        [:span "Due date "]
        [:a {:href (date-path date) }
         (str (:due_date date) " - " (:description date))]])]
   [:div.comments-box
    [:ul.list.comments-list
     (let [comments (comment-db/get-issue-comments (:id issue))]
       (map comment-row comments))]
    (comment-form issue)]
   (if (:open issue)
     (close-issue-form issue)
     (reopen-issue-form issue))])

(defn new [project]
  [:div.center-form
   (breadcrumbs project
                [:li.active
                 [:span "New issue"]])

   (form-to {:class "js-cmd-enter"} [:post (project-path project "/issues/create")]
            [:div.form-group
             (label "description" "Description")
             (text-area {:class "form-control" :rows 6 :required true} "description")]
            (let [users (user-db/get-project-users (:id project))]
              (if (seq users)
                [:div.form-group
                 (label "assignee_id" "Assignee")
                 [:select {:class "form-control" :name "assignee_id"}
                  [:option {:value ""} "None"]
                  (select-options (map user-to-option users) (current-user-id))]]))
            (let [dates (date-db/get-project-dates (:id project) {:future true})]
              (if (seq dates)
                [:div.form-group
                 (label "date_id" "Due date")
                 [:select {:class "form-control" :name "date_id"}
                  [:option {:value ""} "None"]
                  (select-options (map date-to-option dates))]]))
            (submit-button {:class "btn btn-success"} "Create issue"))])

(defn user-issues [issues filter]
  [:div
   [:div.pull-right
    [:a {:class "btn btn-sm btn-default btn-info" :href "/issues/new"}
     [:span "New issue"]]]
   [:h3.page-h "Your issues"]
   [:hr]
   [:div
    [:ul.nav.nav-text-pills
     [:li {:class (if (= "authored" filter) "active")}
      [:a {:href "/issues"} "Created by you"]]
     [:li {:class (if (= "assigned" filter) "active")}
      [:a {:href "/issues/assigned"} "Assigned to you"]]]]
   [:ul.list
    (map issue-row issues)]])

(defn select-project-row [project]
  [:a.list-group-item {:href (str "/project/" (:id project) "/issues/new")}
   [:h4.list-group-item-heading (escape-html (:name project))]
   [:div.description (escape-html (:description project))]])

(defn select-project []
  [:div
   (let [projects (project-db/get-projects-for-user (current-user-id) {:archived false})]
     (if (empty? projects)
       (list
         [:h4 "To create an issue you need a project first."]
         [:a.btn.btn-info {:href "/projects/new"}
          [:strong "New project"]])
       (list
         [:h4 "Select project for new issue"]
         [:div.list-group
          (map select-project-row projects)]
         [:a.btn.btn-info {:href "/projects/new"}
          [:strong "New project"]])))])
