(ns checked.routes.files
  (:require [compojure.core :refer :all]
            [checked.helpers :refer :all]
            [checked.access :refer :all]
            [checked.models.project :as db]
            [checked.views.layout :as layout]
            [checked.views.files :as view]
            [ring.util.codec :refer [url-decode]]
            [ring.util.response :refer [file-response]]
            [noir.util.route :refer [def-restricted-routes]]
            [noir.response :as resp]))


(defn files-page [project-id]
  (let [project (db/get-project project-id)]
    (layout/common (view/files project))))

(defn serve-file [project-id file-name]
  (let [project (db/get-project project-id)]
    (file-response (str uploads-path "/" project-id "/" (url-decode file-name)))))

(def-restricted-routes files-routes
  (GET "/project/:project-id/files" [project-id] (files-page project-id))
  (GET "/files/:project-id/:file-name" [project-id file-name]
       (serve-file project-id file-name)))
