(ns checked.features.home
  (:require [checked.handler :refer [app]]
            [kerodon.core :refer :all]
            [kerodon.test :refer :all]
            [clojure.test :refer :all]))

(deftest homepage-redirect
  (-> (session app)
      (visit "/")
      (follow-redirect)
      (within [:body]
        (has (some-text? "Log in")))))

(deftest not-found
  (-> (session app)
      (visit "/blabla")
      (within [:body]
        (has (some-text? "404. Page not found!")))))
