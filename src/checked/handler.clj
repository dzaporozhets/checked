(ns checked.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [noir.util.middleware :as noir-middleware]
            [ring.logger :as logger]
            [migratus.core :as migratus]
            [noir.session :as session]
            [checked.access :refer :all]
            [checked.routes.home :refer [home-routes]]
            [checked.routes.projects :refer [projects-routes]]
            [checked.routes.files :refer [files-routes]]
            [checked.routes.issues :refer [issues-routes]]
            [checked.routes.comments :refer [comments-routes]]
            [checked.routes.dates :refer [dates-routes]]
            [checked.routes.members :refer [members-routes]]
            [checked.routes.profile :refer [profile-routes]]
            [checked.routes.invites :refer [invites-routes]]
            [checked.routes.admin :refer [admin-routes]]
            [checked.routes.auth :refer [auth-routes]]
            [checked.views.layout :as layout]
            [noir.session :as session]))



(def migratus-config
  {:store :database
   :migration-dir "migrations"
   :db (or (System/getenv "DATABASE_URL") "postgresql://localhost:5432/checked-dev")})

(defn init []
 (do
   (migratus/migrate migratus-config)))

(defn not-found []
  (layout/base
    [:center
     [:h1 "404. Page not found!"]]))


(defn wrap-request-add-app-url [handler]
  (fn add-app-url [{:keys [scheme server-name server-port] :as r}]
    (let [link (str (name scheme) "://" server-name ":" server-port)]
      (handler (assoc r :app-url link)))))

(defroutes app-routes
  (route/resources "/")
  (route/not-found (not-found)))

(def app
  (noir-middleware/app-handler
    [auth-routes
     home-routes
     projects-routes
     files-routes
     issues-routes
     dates-routes
     members-routes
     invites-routes
     comments-routes
     profile-routes
     admin-routes
     app-routes]
    :middleware [wrap-request-add-app-url logger/wrap-with-logger]
    :access-rules [{:rule user-access :redirect "/login"}
                   {:uri "/project/*" :rule project-access :redirect "/404.html"}
                   {:uri "/invite/*" :rule invite-access :redirect "/404.html"}]))
