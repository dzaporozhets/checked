(ns checked.models.issue
  (:require [clojure.java.jdbc :as sql]
            [environ.core :refer [env]]))

(def db
  (env :database-url))

(defn get-project-issues [project_id filter]
  (sql/query db
             ["SELECT * FROM issues
              WHERE project_id = ?
              AND open = ?
              ORDER BY id DESC", project_id, (:open filter)]))

(defn get-date-issues [date_id filter]
  (sql/query db
             ["SELECT * FROM issues
              WHERE date_id = ?
              AND open = ?
              ORDER BY id DESC", date_id, (:open filter)]))

(defn get-user-issues [user_id]
  (sql/query db
             ["SELECT * FROM issues
              WHERE user_id = ?
              AND open = true
              ORDER BY id DESC", user_id]))

(defn get-assigned-user-issues [user_id]
  (sql/query db
             ["SELECT * FROM issues
              WHERE assignee_id = ?
              AND open = true
              ORDER BY id DESC", user_id]))

(defn count-authored-issues [user-id]
  (sql/query db
             ["SELECT COUNT(*) FROM issues WHERE user_id = ? AND open = true", user-id]
             :result-set-fn first))

(defn count-assignee-issues [user-id]
  (sql/query db
             ["SELECT COUNT(*) FROM issues WHERE assignee_id = ? AND open = true", user-id]
             :result-set-fn first))

(defn get-issue-by-id [id]
  (sql/query db
             ["SELECT * FROM issues
              WHERE id = ?", id]
             :result-set-fn first))

(defn get-project-issue [project-id id]
  (sql/query db
             ["SELECT * FROM issues
              WHERE project_id = ?
              AND id = ?", (Integer. project-id), (Integer. id)]
             :result-set-fn first))

(defn update-issue [id params]
  (sql/update! db :issues params ["id = ?" id]))

(defn close-issue [id]
  (sql/update! db :issues {:open false} ["id = ?" id]))

(defn reopen-issue [id]
  (sql/update! db :issues {:open true} ["id = ?" id]))

(defn create-issue [issue]
  (first (sql/insert! db :issues issue)))
