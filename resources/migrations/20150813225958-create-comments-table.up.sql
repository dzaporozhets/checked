CREATE TABLE IF NOT EXISTS comments (
  id    SERIAL PRIMARY KEY,
  comment text NOT NULL,
  user_id integer NOT NULL,
  issue_id integer NOT NULL,
  project_id integer NOT NULL,
  timestamp timestamp DEFAULT current_timestamp
);
