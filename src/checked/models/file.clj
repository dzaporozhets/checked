(ns checked.models.file
  (:require [clojure.java.jdbc :as sql]
            [environ.core :refer [env]]))

(def db
  (env :database-url))

(defn get-file-by-comment [comment-id]
  (sql/query db
             ["SELECT * FROM files WHERE comment_id = ?", comment-id]
             :result-set-fn first))

(defn get-project-files [project-id]
  (sql/query db
             ["SELECT * FROM files WHERE project_id = ? ORDER BY id DESC", project-id]))

(defn create-file [file]
  (first (sql/insert! db :files file)))
