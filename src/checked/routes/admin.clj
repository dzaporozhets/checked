(ns checked.routes.admin
  (:require [compojure.core :refer :all]
            [checked.views.admin :as view]
            [noir.util.route :refer [def-restricted-routes]]
            [checked.views.layout :as layout]))

(defn admin-home []
  (layout/common (view/home)))

(def-restricted-routes admin-routes
  (GET "/admin" [] (admin-home)))
