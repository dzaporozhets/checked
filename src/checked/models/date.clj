(ns checked.models.date
  (:require [clojure.java.jdbc :as sql]
            [environ.core :refer [env]]))

(def db
  (env :database-url))

(defn get-project-dates [project_id filter]
  (if (:future filter)
    (sql/query db
               ["SELECT * FROM dates 
                WHERE project_id = ? 
                AND DATE(due_date) >= CURRENT_DATE 
                ORDER BY id DESC", project_id])
    (sql/query db
               ["SELECT * FROM dates 
                WHERE project_id = ? 
                AND DATE(due_date) < CURRENT_DATE 
                ORDER BY id DESC", project_id])))

(defn get-user-dates [user_id]
  (sql/query db
             ["SELECT * FROM dates WHERE user_id = ?", user_id]))

(defn get-date-by-id [id]
  (sql/query db
             ["SELECT * FROM dates WHERE id = ?", id]
             :result-set-fn first))

(defn create-date [date]
  (first (sql/insert! db :dates date)))
