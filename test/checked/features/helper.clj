(ns checked.features.helper
  (:require [clojure.java.jdbc :as sql]
            [environ.core :refer [env]]
            [checked.models.user :as user]
            [noir.util.crypt :as crypt]
            [kerodon.core :refer :all]
            [kerodon.test :refer :all]
            [clojure.test :refer :all]))

(def db
  (env :database-url))

(defn cleanup-db []
  (sql/delete! db :projects "")
  (sql/delete! db :users ""))

(defn create-user []
  (user/create-user {:name "Foo"
                     :email "foo@example.com"
                     :encrypted_password (crypt/encrypt "123456")}))

(defn login [app]
  (-> app
    (visit "/")
    (follow-redirect)
    (fill-in "Email" "foo@example.com")
    (fill-in "Password" "123456")
    (press "Login")
    (follow-redirect)))
