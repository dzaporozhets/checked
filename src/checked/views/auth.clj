(ns checked.views.auth
  (:require [hiccup.element :refer :all]
            [hiccup.form :refer :all]
            [noir.validation :as vali]
            [noir.session :as session]))

(defn error-item [[error]]
  [:div.text-danger error])

(defn input-control [type id name & [value]]
  [:div.form-group
   (list
     (label id name)
     (vali/on-error (keyword id) error-item)
     (type {:class "form-control" :required true} id value))])

(defn login-page [& [email]]
  [:div.login-form
   [:h3.page-h "Log in"]
   (form-to [:post "/login"]
            (input-control text-field "email" "Email" email)
            (input-control password-field "password" "Password")
            (submit-button {:class "btn btn-success"} "Login"))
   [:p.pull-right
    "Do not have an account? "
    [:a {:class "register" :href "/register"} "Sign Up"]]])



(defn registration-page [& [name email]]
  [:div.registration-form
   [:h3.page-h "Let's create an account"]
   (form-to [:post "/register"]
            (input-control text-field "name" "Name" name)
            (input-control email-field "email" "Email" email)
            (input-control password-field "password" "Password")
            (input-control password-field "password_confirmation" "Repeat password")
            (submit-button {:class "btn btn-success"} "Create account"))])
