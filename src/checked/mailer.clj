(ns checked.mailer
  (:require [compojure.core :refer :all]
            [checked.views.email :as email-view]
            [checked.models.user :as user-db]
            [postal.core :refer [send-message]]
            [environ.core :refer [env]]))

(def email-from
  (env :email-from))

(defn send-comment-email [user author comment project comment-url]
  (send-message {:from email-from
                 :to [(:email user)]
                 :subject (str "[" (:name project) "] New comment")
                 :body [{:type "text/html; charset=utf-8"
                         :content (email-view/new-comment author comment project comment-url)}]}))

(defn send-invite-email [email inviter project]
  (send-message {:from email-from
                 :to [email]
                 :subject (str "[" (:name project) "] You are invited to project")
                 :body [{:type "text/html; charset=utf-8"
                         :content (email-view/invite inviter project)}]}))
