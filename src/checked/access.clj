(ns checked.access
  (:require [compojure.core :refer :all]
            [checked.models.invite :as invite-db]
            [checked.models.issue :as issue-db]
            [checked.models.project :as project-db]
            [checked.models.invite :as invite-db]
            [checked.models.member :as member-db]
            [noir.session :as session]))

(defn invite-access [{:keys [params] :as req}]
  (invite-db/get-user-invite
    (session/get :user-email)
    (Integer. (:id params))))

(defn user-access [_]
  (session/get :user-id))

(defn project-access [{:keys [params] :as req}]
  (if-let [user-id (session/get :user-id)]
    (let [project (project-db/get-project (:project-id params))]
      (member-db/get-project-member
        (:id project)
        (Integer. user-id)))))
