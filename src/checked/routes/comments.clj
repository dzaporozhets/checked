(ns checked.routes.comments
  (:require [compojure.core :refer :all]
            [checked.helpers :refer :all]
            [checked.access :refer :all]
            [checked.models.issue :as issue-db]
            [checked.models.comment :as comment-db]
            [checked.models.member :as member-db]
            [checked.models.project :as project-db]
            [checked.models.user :as user-db]
            [checked.models.file :as file-db]
            [checked.mailer :refer [send-comment-email]]
            [clavatar.core :refer [gravatar]]
            [ring.util.response :refer [response]]
            [noir.util.route :refer [def-restricted-routes]]
            [noir.session :as session]
            [noir.io :refer [upload-file resource-path]]
            [noir.response :as resp]))

(defn create-issue-comment [project-id id comment {:keys [filename size] :as file} app-url]
  (let [issue (issue-db/get-project-issue project-id id)]
    (let [comment (comment-db/create-comment {:user_id (current-user-id)
                                                     :project_id (Integer. project-id)
                                                     :issue_id (Integer. id)
                                                     :comment comment})]
      (if (seq filename)
        (try
          (noir.io/upload-file (str uploads-path project-id) file :create-path? true)
          (file-db/create-file {:name filename
                                :size size
                                :project_id (Integer. project-id)
                                :user_id (current-user-id)
                                :comment_id (:id comment)})))
      (let [project (project-db/get-project-by-id (Integer. project-id))]
        (let [comment-url (str app-url (issue-path issue (str "#comment-" (:id comment))))]
          (let [author (user-db/get-user-by-id (current-user-id))]
            (doseq [member (member-db/get-members (Integer. project-id))]
              (if-not (= (:user_id member) (current-user-id))
                (if-let [recipient (user-db/get-user-by-id (:user_id member))]
                  (send-comment-email recipient author comment project comment-url)))))))
      (resp/redirect (issue-path issue "#comment-form")))))

(def-restricted-routes comments-routes
  (POST "/project/:project-id/issue/:id/comment" [project-id id comment file :as {:keys [app-url]}]
        (create-issue-comment project-id id comment file app-url)))
