(ns checked.views.invites
  (:require [hiccup.element :refer :all]
            [checked.models.invite :as db]
            [checked.models.project :as project-db]
            [noir.session :as session]))


(defn invite-row [invite]
  (let [project (project-db/get-project-by-id (:project_id invite))]
    [:li.invite.clearfix
     [:a.name {:href (str "project/" (:id project))}
      [:strong (:name project)]]
     [:div.pull-right
      [:form {:action (str "/invite/" (:id invite) "/accept") :method "POST"}
       [:button {:class "btn btn-sm btn-success"} "Join project"]]
      [:form {:action (str "/invite/" (:id invite) "/decline") :method "POST"}
       [:button {:class "btn btn-sm btn-danger"} "Decline"]]]
     [:div.description (:description project)]]))


(defn invites []
  [:div
   [:h3.page-h "Invites"]
   [:p.light "When you are invited to join the project - it will be displayed here"]
   (let [email (session/get :user-email)]
     [:ul.list
      (let [invites (db/get-invites email)]
        (map invite-row invites))])])
